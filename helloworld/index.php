<?php
/**
 * Index page for the plugin helloworld and request handling 
 *
 * @package plugins tutorial
 * @author      Chama EL KASRI <chama@entornos.net>
 * @copyright  2020 Chama EL KASRI 
 
 */
require_once('../../config.php');
require_once('local_forms.php');

$LOCAL_PATH = '/local/helloworld/index.php';
$context = context_system::instance();

function add_record($message) {
    global $DB;
    global $USER;
    $date = new DateTime("now", core_date::get_user_timezone_object()); 
    $timestamp= $date->getTimestamp();
    $post_msg = (object)array('message' => $message, 'timecreated' => $timestamp, 'userid' => $USER->id);
    $DB->insert_record('local_helloworld_msgs',$post_msg);

}

function create_post(){

    $key_form = 'descripcion';
    $mform = new wall_form();
    if ($data = $mform->get_data()) {
        $text = $data->{$key_form};
        add_record($text);

    }
    return $mform;
}

$PAGE->set_context($context);
$PAGE->set_pagelayout('standard');
require_login();

if(isloggedin() and isguestuser()){
    print_error('noguest');    
}else{    
    $PAGE->set_url($LOCAL_PATH);
}

$username_param = 'username'; 
$username = optional_param($username_param,'', PARAM_ALPHA); // User name

if (strlen($username) != 0){
    $PAGE->set_heading(get_string('hellouser', 'local_helloworld', $username));
}else{
    $PAGE->set_heading(get_string('sayhello','local_helloworld'));
}

echo $OUTPUT->header();
$post = create_post();
$post->display();
require_once('card_column.php');
echo $OUTPUT->footer();
