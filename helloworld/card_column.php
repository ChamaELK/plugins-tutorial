<?php

/**
 *  
 * Card column
 * @package plugins tutorial
 * @author      Chama EL KASRI <chama@entornos.net>
 * @copyright  2020 Chama EL KASRI 
 
 */

defined('MOODLE_INTERNAL') || die();

function delete_button($url,$id,$context){
    $button = NULL;
    if(has_capability('local/helloworld:deleteanymessage',$context)){
          $action = new moodle_url($url, ['delete' => $id, 'sesskey' => sesskey()]);
          $button = html_writer::link($action, get_string('delete'));     
      }
      return $button;
}


global $DB;
$context = context_system::instance();
$id = optional_param('delete','', PARAM_INT);
if($id ==! 0 and has_capability('local/helloworld:deleteanymessage',$context)){
    if($DB->record_exists_select('local_helloworld_msgs','id='.$id)){
        require_sesskey();
        $DB->delete_records_select('local_helloworld_msgs','id='.$id);
    }
}

$userfields = get_all_user_name_fields(true, 'u');
$sql = "SELECT m.id, m.message, m.timecreated, u.id AS userid, $userfields
          FROM {local_helloworld_msgs} m
     LEFT JOIN {user} u ON u.id = m.userid 
      ORDER BY timemodified DESC";
$records = $DB->get_records_sql($sql);

$messages = array();
foreach ($records as $i => $value) {
    $unixtime = $value->timecreated;
    $date = new DateTime();
    $date->setTimestamp(intval($unixtime));
    $time = userdate($date->getTimestamp()); 
    $obj = new stdClass();
    $obj->message = $value->message;
    $obj->time = userdate($date->getTimestamp());  
    $obj->messageid = $value->id; 
    $obj->username = $value->firstname;
    $messages[] = $obj;
}

?>


<?php foreach($messages as $i => $value){ ?>
    <div class="card p-3" style = "width: 18rem;">
    <blockquote class="blockquote mb-0 card-body">
      <p><?php echo $value->message; ?></p>
      <p class="card-text"><small class="text-muted"><?php echo $value->username; ?></small></p>
      <footer class="blockquote-footer">
        <small class="text-muted">
          <?php echo $value->time; ?>
        </small>
      </footer>
      <?php echo delete_button($LOCAL_PATH, $value->messageid,$context); ?>
    </blockquote>
    </div>
    <?php    
}
?>



