<?php
/**
 * Create a simple html form class for the user to enter it's name
 *
 * @package plugins tutorial
 * @author      Chama EL KASRI <chama@entornos.net>
 * @copyright  2020 Chama EL KASRI 
 *
 */

require_once($CFG->libdir. '/formslib.php');


/**
*
* 
* Form with input text : type your name
* Submit button
*/

class user_form extends moodleform {
    public function definition() {
        global $CFG;
        
        $mform = $this->_form;
        $mform->addElement('text', 'name', 'What is your name ?'); 
        $mform->setType('name', PARAM_NOTAGS);                   
        $mform->setDefault('name', 'type your name');
        
        
    }
    function validation($data, $files) {
        return array();
    }

}


/**
*
* 
* Form used in the message wall
* Submit button
*/


class wall_form extends moodleform {

    public function definition() {
        global $CFG;
        
        $mform = $this->_form;
        
        $mform->addElement('textarea','descripcion');
        $mform->setType('descripcion', PARAM_TEXT);
        $mform->setDefault('descripcion','Type your message !');
        $this->add_action_buttons($cancel=false, $submitlabel= get_string('submit', 'moodle')); 
        
    }
    function validation($data, $files) {
        return array();
    }

}

