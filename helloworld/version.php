<?php 
/**
 * Provides metadata
 *
 * @package plugins tutorial
 * @author      Chama EL KASRI <chama@entornos.net>
 * @copyright  2020 Chama EL KASRI 
 *
 */

$plugin->component = 'local_helloworld';
$plugin->version = 20200818004;
$plugin->requires = 2020061503.06; 
