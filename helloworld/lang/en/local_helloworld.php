<?php
/**
 * Defines the english string used by the plugin helloworld
 *
 * @package plugins tutorial
 * @author      Chama EL KASRI <chama@entornos.net>
 * @copyright  2020 Chama EL KASRI 
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Hello world';
$string['hellouser'] = 'Hello {$a}';
$string['sayhello'] = 'Hello world';
$string['manage'] = 'Manage Hello world';
$string['showinnavigation'] = 'Show in navigation';
$string['showinnavigation_desc'] = 'When enabled, the site navigation will display a link to the Hello world plugin main page';
$string['helloworld:deleteanymessage'] = 'Delete any posted message from the Hello world wall';
$string['helloworld:postmessages'] = 'Post new messages to the Hello world wall';
$string['helloworld:viewmessages']= 'View messages on the Hello world wall';

