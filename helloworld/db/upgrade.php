<?php
/**
 * upgrade database 
 *
 * @package plugins tutorial
 * @author      Chama EL KASRI <chama@entornos.net>
 * @copyright  2020 Chama EL KASRI 
 
 */
function xmldb_20200818002_upgrade() {
 
    global $DB;
 
    $dbman = $DB->get_manager(); 
    if ($oldversion < 20200818002) {

        // Define table local_helloworld_msgs to be created.
        $table = new xmldb_table('local_helloworld_msgs');

        // Adding fields to table local_helloworld_msgs.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('message', XMLDB_TYPE_TEXT, null, null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table local_helloworld_msgs.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, ['id']);
        $table->add_key('fk_msgs_userid', XMLDB_KEY_FOREIGN, ['userid'], 'user', ['id']);

        // Conditionally launch create table for local_helloworld_msgs.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Helloworld savepoint reached.
        upgrade_plugin_savepoint(true, 20200818002, 'local', 'helloworld');
    }
}

xmldb_20200818002_upgrade(); 

